/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a07t01;

/**
 *
 * @author Igor
 */
public class A07t01 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Fabricante logitech = new Fabricante("Logitch");
        Fabricante dell = new Fabricante("Dell");
        Fabricante samsung = new Fabricante("Samsung");
        
        Produto hd = new Produto("HD", "400-AJOQ", dell);
        Produto teclado = new Produto("TECLADO", "B-VX120", samsung);
        Produto mouse = new Produto("MOUSE", "G402", logitech);
     
        System.out.println(hd.getNome()+" "+hd.getModelo()+" "+hd.getFabricante().getNome());
        
    }
    
}
